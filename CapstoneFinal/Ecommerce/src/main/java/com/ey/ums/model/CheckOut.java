package com.ey.ums.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CheckOut {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String name;
	private String address;
	private Long cardNumber;
	private int cvv;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Long getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(Long cardNumber) {
		this.cardNumber = cardNumber;
	}
	public int getCvv() {
		return cvv;
	}
	public void setCvv(int cvv) {
		this.cvv = cvv;
	}
	public CheckOut(int id, String name, String address, Long cardNumber, int cvv) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.cardNumber = cardNumber;
		this.cvv = cvv;
	}
	public CheckOut() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "CheckOut [id=" + id + ", name=" + name + ", address=" + address + ", cardNumber=" + cardNumber
				+ ", cvv=" + cvv + "]";
	}
	
}
