package com.ey.ums.csvResponseMsg.initializer;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.ey.ums.model.Order;
import com.ey.ums.repository.OrderRepository;

public class Initializer {
	@Autowired
	public OrderRepository repository;

	@PostConstruct
	public void onInit() {
		repository.deleteAll();
		Order ord = new Order();
		ord.setAddress("#67,BANGALORE");
		ord.setName("MAHESH");
		ord.setCardNumber(123456L);
		ord.setCvv("23");
		repository.save(ord);
		ord = new Order();
		ord.setAddress("99,CHENAI");
		ord.setName("MIKE");
		ord.setCardNumber(45214L);
		ord.setCvv("21");
		repository.save(ord);

	}
}
