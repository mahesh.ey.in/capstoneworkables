package com.ey.ums.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;



import com.ey.ums.model.CheckOut;
import com.ey.ums.model.Order;
import com.ey.ums.service.CartService;
import com.ey.ums.service.OrderService;



@Controller
@RequestMapping("/order")
public class OrderController {



   @Autowired
    OrderService orderService;
    @Autowired
    CartService cartService;
    
    @GetMapping("/billGenerate")
    public String billGenerate(Model model) {
        model.addAttribute("total", cartService.getSum());
        Order order = new Order();
        model.addAttribute("checkOut", order);
        return "checkOut";
    }
    @PostMapping("/checkOutBill")
    public String checkOutBill(@ModelAttribute Order order) {
        orderService.saveOrder(order);
        return "Finish";
    }
}