package com.ey.ums.model;

import javax.persistence.Entity; 
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="product")
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long productId;
    private String productName;
    private Long productCost;
    private int productQuantity;
    private String productDescription;
	
    
    public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Long getProductCost() {
		return productCost;
	}
	public void setProductCost(Long productCost) {
		this.productCost = productCost;
	}
	public int getProductQuantity() {
		return productQuantity;
	}
	public void setProductQuantity(int productQuantity) {
		this.productQuantity = productQuantity;
	}
	public String getProductDescription() {
		return productDescription;
	}
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	public Product(Long productId, String productName, Long productCost, int productQuantity,
			String productDescription) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.productCost = productCost;
		this.productQuantity = productQuantity;
		this.productDescription = productDescription;
	}
	@Override
	public String toString() {
		return "Product [productId=" + productId + ", productName=" + productName + ", productCost=" + productCost
				+ ", productQuantity=" + productQuantity + ", productDescription=" + productDescription + "]";
	}
	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}
    
	

}
