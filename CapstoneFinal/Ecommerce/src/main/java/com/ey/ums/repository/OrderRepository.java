package com.ey.ums.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.ey.ums.model.Order;

public interface OrderRepository extends MongoRepository<Order, String> {

}
