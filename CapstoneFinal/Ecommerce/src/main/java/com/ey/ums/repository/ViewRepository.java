package com.ey.ums.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ey.ums.model.Cart;

public interface ViewRepository extends JpaRepository<Cart, Long> {

}
