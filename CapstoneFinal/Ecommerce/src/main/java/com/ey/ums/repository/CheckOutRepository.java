package com.ey.ums.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ey.ums.model.CheckOut;

public interface CheckOutRepository extends JpaRepository<CheckOut, Integer>{

}
