package com.ey.ums.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ey.ums.model.Cart;
import com.ey.ums.repository.CartRepository;

@Service
public class CartServiceImpl implements CartService{

	@Autowired
    CartRepository cartRepo;
    @Override
    public List<Cart> getAllCartItems() {
        return cartRepo.findAll();
    }
	@Override
	public void deleteOrder(Long orderid) {
		cartRepo.deleteById(orderid);
		
	}
	@Override
	public void deleteCart() {
		cartRepo.deleteAll();
	
		
	}
	@Override
	public Long getSum() {
	List<Cart> list	= cartRepo.findAll();
	Long sum=0L;
	for(Cart c : list) {
		sum = sum + c.getProduct().getProductCost();
	}
		return sum;
	}


}
