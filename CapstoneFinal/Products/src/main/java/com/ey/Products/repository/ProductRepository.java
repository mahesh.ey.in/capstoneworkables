package com.ey.Products.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ey.Products.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product,Long>{

}
