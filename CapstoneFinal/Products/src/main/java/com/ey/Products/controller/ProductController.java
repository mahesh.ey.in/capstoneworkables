package com.ey.Products.controller;

import java.util.List; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ey.Products.entity.Product;
import com.ey.Products.service.ProductService;

@RestController
@RequestMapping("product")
public class ProductController {
	
	@Autowired
	ProductService productService;
	
	
	@GetMapping("/show")
	public List<Product> getAllProducts(){
		
		
		return productService.getAllProducts();
		
	}
	
	@PostMapping("/addProducts")
	public String addProducts(@RequestBody Product product) {
		
		productService.addProducts(product);
		return"done boss";
		
		
	}
	@DeleteMapping("/deleteProductById/{productId}")
	public String deleteProduct(@PathVariable("productId")Long productId) {
		productService.deleteProductById(productId);
		return "done Boss";
	}
	

	
}
